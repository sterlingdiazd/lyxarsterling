package org.lyxar.envelope;

import org.lyxar.envelope.sp.BrokerService;
import org.lyxar.envelope.sp.BrokersServices;

/**
 * 
 * <p>
 * Each provider is considered a potential broker that the system
 * could make use of.
 * </p>
 * 
 * <p>
 * Broker provide the ability to serve a simple service that could be
 * backed by many providers, that means that you can have one access 
 * point to a different endpoints only switching providers returned by 
 * {@link BrokerService}	 
 * </p>
 * <p>
 * To retrieve a single broker you need a system {@link Broker}, 
 * once you get it, you can use whichever provider, that was previously 
 * loaded based on configuration broker endpoint.
 * </p>
 * 
 * <p>
 * Once you have desired broker, you can begin to request services on
 * it, specifying the service name. using {@link #getService(String)}
 * </p>
 * 
 * <blockquote>
 * <pre>
 * Broker mBroker = BrokerService.getBrokerById( "any-provider-name" );
 * Service login = mBroker.getService( "/login" );
 * System.out.println( login ); 
 * </pre>
 * </blockquote>
 * 
 * @see BrokerService
 * @see BrokersServices
 * 
 * @since 1.0
 *  
 * @author Jansel R. Abreu (Vanwolf) 
 */
public abstract class Broker {	 

	/**
	 * 
	 * @return provider identification on the server.
	 */
	public abstract String getId();
	
	/**
	 * 
	 * @return the name of the provider. 
	 */
	public abstract String getName();
	
	/**
	 * 
	 * @return public provider host that this instance was binded
	 */
	public abstract String getHost();
	  
	/**
	 * 
	 * @param service name, to execute a fetch process, once reached, 
	 * 		  the {@link Service} instance is returned. 
	 * @return instance of {@link Service}
	 */
	public abstract Service getService( String service );
}
