package org.lyxar.envelope;

import java.util.List;

import org.lyxar.envelope.sp.BrokersServices;

import android.content.Context;
/**
 * <p>
 * System need to be in stable state before to client try fetch providers,
 * where invariant state is guarantee by the {@link InvariantChecker} entity.
 * </p>
 * <p>
 * 	{@link InvariantChecker} based on configuration file <code>scrowlex.properties</code>
 * 	on key <code>com.lyxar.WSERVICE_VERSION</code> fetch version state of remote providers
 *  and try to download providers if local representation are missing, try to update 
 *  providers if the version state differ from the previously loaded,Thats means, {@link InvariantChecker}}
 *  maintain a consistent state between remote and local representation of
 *  all providers that the system use. 
 * </p>
 * <p>
 * 	Once version state are in stable state, {@link CompletionHandler} registered
 *  entity take place to advise termination process.
 * </p>
 * 
 * <p>
 * 	To use {@link InvariantChecker} entity you must retrieve from the default system provider 
 * 	service, one you got it, you can begin the process, calling {@link #resolve(Context, CompletionHandler)}
 *  method on returned default Checker.  
 * </p>
 * 
 * <blockquote>
 * <pre>
 * BrokersServices.defaultInvariantChecker().resolve(context, new CompletionHandler(){
 * 	<code>@Override</code>
 * 	public void onCompleted(){
 * 	List<Broker> providers = BrokersServices.defaultBrokerService(this).getBrokers();
 * 	...
 * 	}
 * }
 * </pre>
 * </blockquote>
 * 
 * 
 * @see BrokersServices
 * @see CompletionHandler
 * 
 * @since 1.0
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public abstract class InvariantChecker {
	/**
	 * 
	 * @param cxt
	 * @param handler
	 */
	public abstract void resolve(Context cxt,CompletionHandler handler );	
}
