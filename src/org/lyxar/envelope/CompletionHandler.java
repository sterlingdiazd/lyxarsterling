package org.lyxar.envelope;

import org.lyxar.envelope.sp.BrokerService;
import org.lyxar.envelope.sp.BrokersServices;


/**
 * <p
 * Each time provider fetch process take over, the completion 
 * handler registered at that moment, is called to indicate that 
 * the process is done, and all providers available for consume, 
 * are ready to use.
 * </p>
 * <p>
 * Once the {@link InvariantChecker#resolve(android.content.Context, CompletionHandler)}
 * is completed, all providers are loaded and ready to use, and you can retrieve providers
 * through {@link BrokerService} class.
 * </p>
 * 
 * @see InvariantChecker
 * @see BrokersServices
 * 
 * @since 1.0
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 */
public interface CompletionHandler {	
	/**
	 * <p>
	 * This callback is executed after provider fetch process is done
	 * to indicate that all providers are ready to receive service lookup 
	 * calls.
	 * </p>
	 */
	public abstract void onCompleted();
}
