package org.lyxar.envelope;
/**
 *<p> 
 * Each functionality that provider offer as public manner,
 * is exposed as services that are represented by {@link Service}
 * entity.
 * </p>
 * <p>
 * Once services are retrieved it's possible to start use of it representation,
 * retrieve service after identified {@link Broker} desired. 
 * </p>
 *
 * @see Broker
 * 
 * @since 1.0
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public abstract class Service {

	/**
	 *
	 * @return the associated {@link Broker} that serve sibling services
	 */
	public abstract Broker broker(); 

	/**
	 * 
	 * @return unique identifier of service, that id come from remote endpoints.
	 */
	public abstract String getId();
	
	/**
	 * 
	 * @return name of service, often referred as resource name
	 */
	public abstract String getName();
	
	/**
	 * 
	 * @return URI that represent this services as remote resource, in that precise moment
	 * 		   this field store resource name in remote server. e.g [service name].[any extension]
	 */
	public abstract String getURLString();
	
	/**
	 * 
	 * @return URI path to physical resource, e.g /specific/path/resources/ 
	 */
	public abstract String getPath();
	
	/**
	 * 
	 * @return query params needed to access that resource, where act as key value pair
	 * 		   values are encoded as question mark that indicate the system must perform 
	 * 		   some value substitution. e.g param1=?&param2=?
	 */
	public abstract String getQueryString();
	
}
