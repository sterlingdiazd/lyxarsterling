package org.lyxar.envelope.sp;

import java.util.List;

import org.lyxar.envelope.Broker;
import org.lyxar.envelope.InvariantChecker;
/**
 * <p>
 * Providers are represented by system brokers that are capable
 * to provide all of it functionality as services resources.
 * </p>
 * <p>
 *  {@link BrokerService} represent a collection of {@link Broker} 
 *  stored as key value pair, that entity bring the possibility to 
 *  search providers by it remote identifier or by it name.
 * </p>
 * <p>
 * 	You cannot create Broker service in self own, but you can register a new 
 *  Broker service via configuration file <code>scrowlex.properties</code>
 *  under the key <code>com.lyxar.SYSTEM_BROKER</code>, that key is missing if
 *  the value is empty or key is commented. and that broker is turned as system 
 *  broker.
 *  </p>
 *  
 *  <blockquote>
 *  <pre>
 *   List<Broker> providers = BrokersServices.defaultBrokerService(context).getBrokers();
 *  </pre>
 *  </blockquote>
 *  
 *  @see InvariantChecker
 *  @see BrokersServices
 *  @see Broker 
 * 
 *  @since 1.0
 *  
 *  @author Jansel R. Abreu (Vanwolf) 
 *
 */
public abstract class BrokerService {		  

	/**
	 * 
	 * @param id remote provider identifier
	 * @return {@link Broker} that id belong to
	 */
	public abstract Broker getBrokerById( String id );
		
	/**
	 * 
	 * @return all {@link Broker} served by this provider
	 */
	public abstract List<Broker> getBrokers();
	
}
