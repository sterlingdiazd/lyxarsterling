package org.lyxar.envelope.sp;

import org.lyxar.envelope.InvariantChecker;

import rowk.lyxar.envelope.imp.KBrokerServiceResolver;
import rowk.lyxar.envelope.imp.KInvariantCheckerImp;
import android.content.Context;

/**
 * <p>
 * When system need to load and retrieve the default {@link BrokerService}
 * this class is used for.
 * </p>
 * <p>
 *	Default system {@link BrokerService} is a broker service that attempt to 
 *	attend all request of specifics providers. default provider serve all 
 *  provider which have a remote representation, if you need to change this behavior
 *  you need to provide a custom {@link BrokerService} and register in configuration file
 *  <code>scrowlex.properties</code> under the key <code>com.lyxar.SYSTEM_BROKER</code>.
 * </p>
 * <p>
 * 	Some policy are required to accomplish when system provider are substituted:
 * 	<ul>
 * 		<li>{@link BrokerService} must have a public constructor that receive unique param type of <code>Context</code></li>
 * 		<li>Must be a public class</li>
 * 		<li>Must extend {@link BrokerService} class</li>
 *  </ul>  
 * </p>
 * <p>
 * 	If some of this rules are violated o the for other reason the instantiation could not be possible,
 *  the runtime {@link BrokerService} loader @throws ScrowlexConfigException.
 * </p>
 * <p>
 * 	By default the configuration key <code>com.lyxar.SYSTEM_BROKER</code> is commented, to enable 
 *  handled {@link BrokerService}.
 * </p>
 * 
 * @see BrokerService
 * @see InvariantChecker
 * 
 * @since 1.0
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */

public class BrokersServices {	
	
	/**
	 *  This method return default system service.
	 *  
	 * @param context that android pass to service loader 
	 * @return default system service.
	 */
	public static BrokerService defaultBrokerService( Context context ){
		return new KBrokerServiceResolver( context ).resolved();
	}
	
	
	/**
	 * retrieve default {@link InvariantChecker} entity
	 * 
	 * @return Checker
	 */
	public static InvariantChecker defaultInvariantChecker(){
		return new KInvariantCheckerImp();
	}
	
}
