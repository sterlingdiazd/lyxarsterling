package org.lyxar.envelope;
/**
 * <p>
 * System state is guarantee by this remote version, indicating if system
 * is up to date related to providers, or need to be updated.
 * </p>
 * <p>
 * 	By default the you rarely need to interact with versions, because versions
 *  are part of {@link InvariantChecker} process, the implementation is free to use
 *  for log scenarios.
 * </p>
 * 
 * @see InvariantChecker
 * 
 * @since 1.0
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public class Version {
	
	private boolean requireUpdate;
	
	private String brokerKey;
	
	public Version(){}
	
	/**
	 * 
	 * @return indicate if system need to be updated with remote representation.
	 */
	public boolean isRequiredUpdate() {
		return requireUpdate;
	}


	public void setRequiredUpdate(boolean requireUpdate) {
		this.requireUpdate = requireUpdate;
	}

	public String getBrokerKey() {
		return brokerKey;
	}

	public void setBrokerKey(String brokerKey) {
		this.brokerKey = brokerKey;
	}

	
	
}
