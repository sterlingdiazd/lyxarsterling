package org.lyxar.exception;

import java.util.MissingResourceException;


/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
@SuppressWarnings("serial")
public class ScrowlexConfigException extends MissingResourceException{
	public ScrowlexConfigException( String msg,String className,String resource ){
		super( msg, className,resource ); 
	}
}
