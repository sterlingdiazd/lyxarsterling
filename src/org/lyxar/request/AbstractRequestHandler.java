package org.lyxar.request;

import android.content.Context;


/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public abstract class AbstractRequestHandler<T> implements Handleable<T>{
	
	protected Handleable<T> nextInChain;
	protected DeviceState deviceState;
	protected Context context;
	
	public AbstractRequestHandler( Context context,Handleable<T> nextInChain,DeviceState deviceState )
	{
		this.context = context;
		this.nextInChain = nextInChain;
		this.deviceState = deviceState;
	}
	
	public abstract boolean canResolve();
	
	
    public void setNextInChain( Handleable<T> handleable ){
		this.nextInChain = handleable;
	}
    
    public Handleable<T> getNextInChain(){
    	return nextInChain;
    }
    
}
