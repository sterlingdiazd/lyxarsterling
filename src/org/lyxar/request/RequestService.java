package org.lyxar.request;

import java.util.List;


import android.content.Context;



/**
 *
 * @param <T> type of data must be retrieved from teh database or server.
 * 
 * <p>
 * This class serve as base class for the specific request.
 * <p>
 * The concept of this class is deliver the responsibility  of retrieve data from any Data Storage if and only if
 * the actual class can't perform seek data operation. One case is when any <code>LocalRequest<code> Can't retrieve data from
 * Database for any reason.then the <code>LocalRequest</code> should leverage the responsibility to <code>RemoteRequest</code>
 * and if <code>RemoteRequest</code> can't perform load data process, then should leverage the next in chain if this is not <code>null</code>
 * object.  
 * 
 */

/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public abstract class RequestService< T > {
	
	protected Handleable<T> gangHandlers;
	protected Handleable<T> currentHandler;
	protected DeviceState mDevState;	
	protected Context context;
	
	public RequestService( Context context,DeviceState deviceState,Handleable<T> gangHandler ){
		this.mDevState = deviceState;		
		this.context = context;
		this.gangHandlers = gangHandler;
	}
	
	
	protected abstract Handleable<T> getHandler( DeviceState deviceState );
	
	
	public Handleable<T> getGangHandler(){
		return gangHandlers;
	}

	
	public void setGangHandler( Handleable<T> gangHandler ){
		this.gangHandlers = gangHandler;
	}
	
	public Handleable<T> getCurrentHandler() {
		return currentHandler;
	}
	
	
	public void setCurrentHandler(Handleable<T> initialHandler) {
		this.currentHandler = initialHandler;
	}
	
	
	public final List<T> load( Object...params ){
		
		Handleable<T> handleable = getHandler( mDevState );		
		return handleable == null ? null : handleable.handle( params );
	}
	
	
}
