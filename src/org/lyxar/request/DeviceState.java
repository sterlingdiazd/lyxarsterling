package org.lyxar.request;

import java.io.Serializable;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public enum DeviceState implements Serializable{
	AVAILABLE, 
	UNAVAILABLE,
	UNREACHABLE;
}
