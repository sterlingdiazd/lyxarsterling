package org.lyxar.request;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public class RequestAsyncTask< T > extends AsyncTask<Object, Float, List< T > >{

	private RequestService< T > requestService;
	private List<Observer> observers;
	private AsyncTaskOservable observable;
	
	private class AsyncTaskOservable extends Observable
	{
		@Override
		public void addObserver(Observer observer) {
			observers.add(observer);
		}
		@Override
		public void notifyObservers(Object data) {
			for( Observer obs : observers )
				obs.update(this, data);
		}
	}
	
	public RequestAsyncTask(Context context, RequestService< T > requestService ) {
		super();
		
		this.requestService = requestService;	
		
		observers  =  new ArrayList<Observer>();
		observable =  new AsyncTaskOservable();
	}
	
	
	
	@Override
	protected List< T > doInBackground(Object... params) {
		Handleable< T > handler = null;
		List<T> emptyPayload = new ArrayList<T>();
		do{			
			try{			
				List< T > payloadData = requestService.load( params );
				if( payloadData != null )
					return payloadData;
				
			}catch( Exception ioe ){
				Log.d( "RequestAsyncTaskException:doInBackground:Exception",ioe.toString()+" , Trying to recover with other Handleable" );				
				handler = requestService.getCurrentHandler();
				handler = handler != null ? ((AbstractRequestHandler<T>)handler).getNextInChain() : null;
				requestService.setCurrentHandler(handler);
			}
			
		}while( handler != null );
		
		Log.d( "RequestAsyncTask:doInBackground", "There aren't handler to resolve the request" );
		return emptyPayload;
	}

	
	@Override
	protected void onCancelled() {
		super.onCancelled();
	}

	
	
	@Override
	protected void onPostExecute(List< T > result) {
		super.onPostExecute(result);
		observable.notifyObservers(result);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(Float... values) {
		super.onProgressUpdate(values);
		if( !isCancelled() )
			return;

	}
	
	public void addObserver(Observer observer) {
		observable.addObserver(observer);
	}	
	

}
