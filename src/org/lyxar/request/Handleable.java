package org.lyxar.request;

import java.util.List;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public interface Handleable<T> {

	public List<T> handle( Object...args );
}
