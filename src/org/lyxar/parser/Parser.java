package org.lyxar.parser;

/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 * @param <Param>
 * @param <Return>
 */
public abstract class Parser<Param, Return> {

	public abstract Return parse(Param... params);

}
