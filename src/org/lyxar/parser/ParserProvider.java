package org.lyxar.parser;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.Properties;
import java.util.WeakHashMap;

import android.content.Context;
import android.util.Log;

/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public class ParserProvider {

	private static final String TAG = ParserProvider.class.getSimpleName();
	public static final String ASSET_CONFIG_FILE = "parsers.properties";

	private static Properties parsers;

	private static WeakHashMap<String, WeakReference<Parser<?, ?>>> loadedParsers;

	public static void loadAssetsParsers(Context context) {
		try {
			InputStream stream = context.getAssets().open(ASSET_CONFIG_FILE);
			parsers = new Properties();
			parsers.load(stream);
		} catch (IOException ioe) {
			Log.d(TAG, "Can't open fasset file resource: " + ioe.toString());
		}
	}

	@SuppressWarnings("unchecked")
	public static <TParam, TReturn> Parser<TParam, TReturn> getApplicationParser(
			String name) throws Exception {

		if (parsers == null)
			throw new IllegalStateException("Parsers Must be Loaded");

		String className = parsers.getProperty(name);

		if (className == null)
			throw new IllegalArgumentException(
					"The Specified Parser is not registered, try to register mannualy or via parsers.properties");

		if (loadedParsers == null)
			loadedParsers = new WeakHashMap<String, WeakReference<Parser<?, ?>>>();

		WeakReference<Parser<?, ?>> weakReference = loadedParsers.get(name);
		Object finalParser = null;

		boolean needToBeCreated = false;

		if (weakReference != null) {
			finalParser = (Parser<TParam, TReturn>) weakReference.get();

			if (finalParser == null) {
				needToBeCreated = true;
			}
		} else
			needToBeCreated = true;

		if (needToBeCreated) {
			Class<?> loadedClass;
			try {
				loadedClass = Class.forName(className);
			} catch (ClassNotFoundException e) {
				throw e;
			}

			try {
				finalParser = loadedClass.newInstance();
			} catch (InstantiationException e) {
				throw e;
			} catch (IllegalAccessException e) {
				throw e;
			}

			if (finalParser != null) {
				weakReference = new WeakReference<Parser<?, ?>>((Parser<?, ?>) finalParser);
				loadedParsers.put(name, weakReference);
			}
		}

		return (Parser) finalParser;
	}

	
	
	public static void registerParser(String name, String className) {
		if (parsers == null)
			parsers = new Properties();
		parsers.setProperty(name, className);
	}

}
