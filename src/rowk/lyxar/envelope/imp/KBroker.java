package rowk.lyxar.envelope.imp;

import java.util.HashMap;

import org.lyxar.envelope.Service;

/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */ 
class KBroker extends org.lyxar.envelope.Broker{

	 String id;
	 String name;
	 String host;
	 HashMap<String, Service> services;
		
	
	@Override
	public Service getService(String service) {		
		return services.get(service);
	}

	
	@Override
	public String getId() {
		return id;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getHost() {
		return host;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();		
		builder.append(  "broker{ id: " ).append( id ).append( "," )
				.append( "name: " ).append( name ).append( "," )
				.append( "host: " ).append( host ).append( "," )
				.append( "services:{ ").append( services.values().toString() ).append( " } }" );		
		return builder.toString();
	}

}




