package rowk.lyxar.envelope.imp;

import java.io.InputStream;
import java.util.Properties;

import org.lyxar.exception.ScrowlexConfigException;

import android.content.Context;
import android.util.Log;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
class ScrowlexLoader {
	
	private static ScrowlexConfig config;

	public static ScrowlexConfig loadConfig(Context cxt, String filename) {
		
		if( config != null )
			return config;
		
		Properties brokers = null;
		try{
			
			Log.w(Common.TAG, "Trying to Load scrowlex config file..." );			
			InputStream stream = cxt.getAssets().open( filename );
			
			brokers = new Properties();
			brokers.load(stream);
			
			config = new ScrowlexConfig();
			attachProperties(brokers);
			
			Log.w(Common.TAG, config.toString() );
			
		}catch( Exception ex ){
			throw new ScrowlexConfigException( "Scrolex configuration file is missing, Caused:"+ex.toString(),ScrowlexConfig.class.getCanonicalName(),ScrowlexConfig.ASSET_CONFIG_FILE);
		}
		return config;
	}
	
	
	private static void attachProperties( Properties prop ){
		if( config == null )
			return;
		
		config.setSystemBrokerService( prop.getProperty( ScrowlexConfig.SYSTEM_BROKER_SERVICE, "" ) );
		config.setWsBrokers( prop.getProperty( ScrowlexConfig.SYSTEM_BROKERS_WS, "") );
		config.setWsVersion( prop.getProperty( ScrowlexConfig.SYSTEM_VERSION_WS, "" ) );		
	}
}
