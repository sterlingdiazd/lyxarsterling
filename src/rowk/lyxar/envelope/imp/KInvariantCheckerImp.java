package rowk.lyxar.envelope.imp;

import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.lyxar.envelope.CompletionHandler;
import org.lyxar.envelope.InvariantChecker;
import org.lyxar.envelope.Version;
import org.lyxar.exception.ScrowlexConfigException;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public class KInvariantCheckerImp extends InvariantChecker{

	
	private CompletionHandler handler;
	private Context context;
	ScrowlexConfig config;
	
	
	@Override
	public void resolve(final Context cxt, CompletionHandler handler) {		
		this.handler = handler;
		this.context = cxt;
		this.config = ScrowlexLoader.loadConfig(cxt, ScrowlexConfig.ASSET_CONFIG_FILE );
				
		VersionAsyncTask task = new VersionAsyncTask();
		task.execute();
	}

	
		
	private void onVersionResolved( Version version ) {
		boolean requireUpdate = true;
		
		if( version != null ){
			requireUpdate = version.isRequiredUpdate();
		}
		
		if( requireUpdate ){
			resolveDependentBrokers();
		}else
			onCheckCompletion();
	}

	
	
	private void onCheckCompletion(){
		if( handler != null )
			 handler.onCompleted();
	}
	
	
	private void resolveDependentBrokers(){
		BrokerAsyncTask task = new BrokerAsyncTask();
		task.execute();
	}

	
	
	private void saveBrokers( String brokers ){
		SharedPreferences prefs = context.getSharedPreferences(Common.ENVELOPE_PREFERENCES, Context.MODE_PRIVATE );
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(Common.PREFS_BROKERS_KEY, brokers);
		editor.commit();
	}
	
	
	
	private class VersionAsyncTask extends AsyncTask<String, Void, Version>{

		@Override
		protected Version doInBackground(String... params) {	
			String jsonResponse;
			
			try{
				
				Log.w(Common.TAG, "Trying to fetch remote version data..." );				
				
				String url = config.getWsVersion();	
				
				HttpClient httpClient = new DefaultHttpClient();
				URI uri = new URI(url);
				HttpGet httpGet = new HttpGet(uri);
				
				HttpResponse httpResponse = httpClient.execute(httpGet);
				jsonResponse = EntityUtils.toString(httpResponse.getEntity());	
				
				Log.w(Common.TAG, "version:[ "+jsonResponse+" ]" );
				
			}catch( Exception ex ){
				ex.printStackTrace();
				throw new ScrowlexConfigException("Service URL is missing or malformed, Caused:"+ex.toString(), VersionAsyncTask.class.getCanonicalName(), ScrowlexConfig.ASSET_CONFIG_FILE+" : "+ScrowlexConfig.SYSTEM_VERSION_WS);
			}
			
			Version remoteVersion = JsonMapper.mapVersion(jsonResponse);
			
			if( remoteVersion != null ){
				SharedPreferences prefs = context.getSharedPreferences( Common.ENVELOPE_PREFERENCES, Context.MODE_PRIVATE);
				String localVersions = prefs.getString( Common.PREFS_VERSION_KEY, "" );
				
				boolean requireUpdate = false;
				
				if( localVersions.equals("") )
					requireUpdate = true;
				else{
					Version localVersion = JsonMapper.mapVersion(localVersions);
					if( !remoteVersion.getBrokerKey().equals(localVersion.getBrokerKey()) )
						requireUpdate = true;										
				}
				
				if( requireUpdate ){
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString(Common.PREFS_VERSION_KEY, jsonResponse);
					editor.commit();
				}
				
				remoteVersion.setRequiredUpdate( requireUpdate );
			}			
			return remoteVersion;
		}
		
		
		
		@Override
		protected void onPostExecute(Version result) {
			onVersionResolved(result);
		}
		
	}
	
	
	private class BrokerAsyncTask extends AsyncTask<String, Void, String>{
	
		@Override
		protected String doInBackground(String... params) {
			String jsonResponse;
			
			try{
				Log.w(Common.TAG, "Trying to Update brokers data caused by version change..." );
				
				String url = config.getWsBrokers();	
				
				HttpClient httpClient = new DefaultHttpClient();
				URI uri = new URI(url);
				HttpGet httpGet = new HttpGet(uri);
				
				HttpResponse httpResponse = httpClient.execute(httpGet);
				jsonResponse = EntityUtils.toString(httpResponse.getEntity());	
				
				Log.w(Common.TAG, "brokers: [ "+jsonResponse+" ]" );
				
			}catch( Exception ex ){
				throw new ScrowlexConfigException("Brokers URL is missing or malformed, Caused:"+ex.toString(), VersionAsyncTask.class.getCanonicalName(), ScrowlexConfig.ASSET_CONFIG_FILE+" : "+ScrowlexConfig.SYSTEM_BROKERS_WS);
			}	
			
			return jsonResponse;
		}

		
		@Override
		protected void onPostExecute(String result) {
			saveBrokers(result);
			onCheckCompletion();
		}
		
	}
	
	
}
