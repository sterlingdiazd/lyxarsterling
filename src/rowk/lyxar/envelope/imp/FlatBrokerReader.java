package rowk.lyxar.envelope.imp;

import android.content.Context;
import android.content.SharedPreferences;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
class FlatBrokerReader {

	public static String readFlatBrokers( Context context ){
		SharedPreferences prefs = context.getSharedPreferences(Common.ENVELOPE_PREFERENCES, Context.MODE_PRIVATE);
		return prefs.getString(Common.PREFS_BROKERS_KEY, "" );
	}
}
