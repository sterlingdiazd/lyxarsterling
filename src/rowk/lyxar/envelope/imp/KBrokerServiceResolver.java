package rowk.lyxar.envelope.imp;

import org.lyxar.envelope.sp.BrokerService;
import org.lyxar.exception.ScrowlexConfigException;

import android.content.Context;
import android.util.Log;

/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
public class KBrokerServiceResolver {
	
	
	private Context context;
	private ScrowlexConfig config;
	
	public KBrokerServiceResolver( Context context ) {
		this.context = context;		
		config = ScrowlexLoader.loadConfig(context, ScrowlexConfig.ASSET_CONFIG_FILE ); 
	}
	
	
	private BrokerService getParsedServiceBroker() {
		
		boolean requiredDefault = false;		
		BrokerService service = null;
		
		if (config == null || config.getSystemBrokerService().equals("") )
			requiredDefault = true;

		if (!requiredDefault) {
			String className = config.getSystemBrokerService();

			if (className == null || className.equals(""))
				requiredDefault = true;

			if (!requiredDefault) {
				Class<?> loadedClass = null;
				try {
					
					//Log.w(Common.TAG, "Loading service "+className+"..." );
										
					loadedClass = Class.forName(className);
				} catch (ClassNotFoundException e) {
					requiredDefault = true;
				}

				try {
					
					//Log.w(Common.TAG, "Instantiating service "+className+"..." );
					service = (BrokerService) loadedClass.newInstance();
					
					if( service instanceof KBrokerServiceImp )
						((KBrokerServiceImp) service).context = context;
					
				} catch (Exception ex) {
					requiredDefault = true;
				}
				if (!requiredDefault)
					return service;
				else
					Log.w(Common.TAG, "Imposible to resolve service "+className+"..." );
			}
		}
		
		Log.w(Common.TAG, "Instantiating default service" );
		return new KBrokerServiceImp( context );
	}	
	
	
	
	public  BrokerService resolved(){
		BrokerService brokerService = null;
		try{
			config = ScrowlexLoader.loadConfig(context, ScrowlexConfig.ASSET_CONFIG_FILE);
			brokerService = getParsedServiceBroker();
		}catch( ScrowlexConfigException ioe ){
			brokerService = new KBrokerServiceImp( context );			
		}	
		return brokerService;		
	}
	
}
