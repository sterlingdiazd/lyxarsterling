package rowk.lyxar.envelope.imp;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.WeakHashMap;

import org.lyxar.envelope.Broker;

import android.content.Context;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */

public class KBrokerServiceImp extends org.lyxar.envelope.sp.BrokerService{
	
	private static WeakHashMap<String, WeakReference< Broker>> brokers;
	
	Context context;
		
	
	public KBrokerServiceImp(){
		brokers = new WeakHashMap<String, WeakReference<Broker>>();
	}
	
	
	public KBrokerServiceImp( Context context ){
		this();
		this.context = context;
	}
		
		
	@Override
	public Broker getBrokerById( String id ) {
		return createIfAbsent( id );
	}
	
	@Override
	public List<Broker> getBrokers() {
		return JsonMapper.getMappedBrokers(context);
	}	
	
	
	public Broker createIfAbsent( String id ){		
		Broker broker = null;
		boolean needCreate = true;
		
		if( brokers.containsKey(id) ){
			WeakReference< Broker > refBrokeReference = brokers.get(id);
			if( refBrokeReference.get() == null )
				needCreate = true;
			else{
				broker = refBrokeReference.get();
				needCreate = false;
			}
		}
		
		if( needCreate ){
			broker = JsonMapper.getMappedBroker(context,id);
			brokers.put( id, new WeakReference<Broker>( broker ) );
		}
		return broker;	
	}

}
