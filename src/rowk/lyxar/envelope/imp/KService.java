package rowk.lyxar.envelope.imp;

import org.lyxar.envelope.Broker;

/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
class KService extends org.lyxar.envelope.Service {

	Broker broker;
	String id;
	String name;
	String path;
	String urlString;
	String queryString;

	
	@Override
	public String getId() {
		return id;
	}

	
	@Override
	public String getName() {
		return name;
	}
	
	
	@Override
	public Broker broker() {
		return broker;
	}

	@Override
	public String getURLString() {
		return urlString;
	}

	@Override
	public String getQueryString() {
		return queryString;
	}


	@Override
	public String getPath() {
		return path;
	}

	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append( "service{ id: " ).append( id ).append( "," )
			   .append( "name: " ).append( name ).append( "," )
			   .append( "path: " ).append( path ).append( "," )
			   .append( "query-string: " ).append( queryString ).append( "," )
			   .append( "url: " ).append( urlString ).append( " }" );
		return builder.toString();
	}
}
