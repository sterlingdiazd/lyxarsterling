package rowk.lyxar.envelope.imp;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
class Common {

	public static final String 		TAG					    =	"Lyxar";					
	
	public static final String 		ENVELOPE_PREFERENCES	=	"envelope_preferences";
	
	public static final String 		PREFS_BROKERS_KEY		=	"brokers_prefs_key";
	
	public static final String 		PREFS_VERSION_KEY		=	"versions_prefs_hey";
	
	
	public static  class Keys{
		public static final String 		ID_KEY				=	"id";
		public static final String 		URL_KEY				=	"url";
		public static final String 		NAME_KEY			=	"name";
		public static final String 		HOST_KEY			=	"host";
		public static final String 		PATH_KEY			=	"path";
		public static final String		BROKERS_KEY			=	"providers";
		public static final String 		SERVICES_KEY		=	"services";		
		public static final String 		QUERY_STRING_KEY	=	"query_string";				
		public static final String		BROKER_VERSION_KEY	=	"broker_version";
	}
}
