package rowk.lyxar.envelope.imp;

/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */
class ScrowlexConfig {
	
	static final String ASSET_CONFIG_FILE 		= "scrowlex.properties";
	
	static final String SYSTEM_BROKER_SERVICE	= "com.lyxar.SYSTEM_BROKER";
	
	static final String SYSTEM_BROKERS_WS 		= "com.lyxar.WSERVICE_BROKERS";
	
	static final String SYSTEM_VERSION_WS 		= "com.lyxar.WSERVICE_VERSION";
	
	
	private String systemBrokerService;
	
	private String wsVersion;
	
	private String wsBrokers;

	public String getSystemBrokerService() {
		return systemBrokerService;
	}

	public void setSystemBrokerService(String systemBrokerService) {
		this.systemBrokerService = systemBrokerService;
	}

	public String getWsVersion() {
		return wsVersion;
	}

	public void setWsVersion(String wsVersion) {
		this.wsVersion = wsVersion;
	}

	public String getWsBrokers() {
		return wsBrokers;
	}

	public void setWsBrokers(String wsBrokers) {
		this.wsBrokers = wsBrokers;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append( "scrowlex.properties={ ")
			   .append( SYSTEM_BROKER_SERVICE ).append("=").append( systemBrokerService ).append(",")
			   .append( SYSTEM_BROKERS_WS ).append( "=" ).append( wsBrokers ).append( "," )
			   .append( SYSTEM_VERSION_WS ).append( "=" ).append(  wsVersion ).append( " }" );
		return builder.toString();
	}
	
	
}
