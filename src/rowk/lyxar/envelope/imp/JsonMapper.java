package rowk.lyxar.envelope.imp;

import static rowk.lyxar.envelope.imp.Common.Keys.HOST_KEY;
import static rowk.lyxar.envelope.imp.Common.Keys.ID_KEY;
import static rowk.lyxar.envelope.imp.Common.Keys.SERVICES_KEY;
import static rowk.lyxar.envelope.imp.Common.Keys.NAME_KEY;
import static rowk.lyxar.envelope.imp.Common.Keys.BROKERS_KEY;
import static rowk.lyxar.envelope.imp.Common.Keys.QUERY_STRING_KEY;
import static rowk.lyxar.envelope.imp.Common.Keys.PATH_KEY;
import static rowk.lyxar.envelope.imp.Common.Keys.URL_KEY;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lyxar.envelope.Broker;
import org.lyxar.envelope.Service;
import org.lyxar.envelope.Version;

import android.content.Context;
import android.util.Log;
/**
 * 
 * @author Jansel R. Abreu (Vanwolf) 
 *
 */

class JsonMapper {

	static String cachedFlatsBrokers;
	
	
	static List<Broker> getMappedBrokers( Context context ){
		Log.w(Common.TAG, "Retrieving all brokers..." );
		
		return rawMappedBrokers(context, null);
	}
	
	
	
	
	static Broker getMappedBroker( Context context,String brokerId ){
		Log.w(Common.TAG, "Retrieving broker with id="+brokerId );
		
		List< Broker > brokers = rawMappedBrokers(context, brokerId);
		if( brokers != null && brokers.size() > 0 ){
			
			Log.w( Common.TAG, "Retrieved broker "+brokers.get(0) );
			return brokers.get(0);
		}
		return null;
	}
	
	
	
	private static List< Broker > rawMappedBrokers( Context context, String brokerId ){
		if( cachedFlatsBrokers == null )
			cachedFlatsBrokers = FlatBrokerReader.readFlatBrokers(context);
		
		List<Broker> brokers = new ArrayList<Broker>();
		JSONArray providers;
		
		try {
			providers = ( new JSONObject(cachedFlatsBrokers) ).getJSONArray( BROKERS_KEY );
		} catch (JSONException e) {
			throw new IllegalArgumentException( "Expected well formed json brokers, got "+cachedFlatsBrokers );
		}
		
		for( int i=0;i<providers.length();++i ){			
			try {
				KBroker broker = new KBroker();
				
				JSONObject provider = providers.getJSONObject(i);	
				
				String id = getIfNotNull(provider, ID_KEY);
				broker.id = id;
				broker.name = getIfNotNull(provider, NAME_KEY);
				broker.host = getIfNotNull(provider, HOST_KEY);
				
				String strServices = getIfNotNull(provider, SERVICES_KEY );					
				broker.services = mapServices( broker,strServices );
				
				brokers.add(broker);
				
				if( id.equals(brokerId) ){
					brokers.clear();
					brokers.add(broker);
					break;
				}
					
			} catch (JSONException e) {
				throw new IllegalArgumentException( "Expected well formed json brokers:  "+e.getMessage() );
			}			
		}		
		return brokers;		
	}
	
		
	
	
	static HashMap< String,Service > mapServices( KBroker broker,String strServices ){
		
		Log.w(Common.TAG, "Mapping service of broker: "+broker.getName()+", services[ "+strServices+" ]" );
		
		HashMap<String,Service> services = new HashMap<String, Service>();
		
		if( strServices != null && !strServices.equals("") ){
			JSONArray jsonServices;
			
			try {
				jsonServices = new JSONArray(strServices);
			} catch (JSONException e) {
				throw new IllegalArgumentException( "Expected well formed json services, got "+strServices );
			}			
			
			for( int i=0;i<jsonServices.length();++i ){
				try{
					JSONObject service = jsonServices.getJSONObject(i);
					
					KService serv = (KService) mapService(service);
					serv.broker = broker;
					serv.id 	= getIfNotNull(service, ID_KEY );
					serv.name	= getIfNotNull(service, NAME_KEY);					
					serv.path 	= getIfNotNull(service, PATH_KEY);
					serv.urlString 	 = getIfNotNull(service, URL_KEY);
					serv.queryString = getIfNotNull( service,QUERY_STRING_KEY );
										
					services.put( serv.name, serv );
					
				}catch( JSONException ex ){
					throw new IllegalArgumentException( "Expected well formed json services, got "+ex.getLocalizedMessage() );
				}
			}
		}
		
		return services;
	}
	
	
	
	
	static Service mapService( JSONObject service ){
		
		KService serv = null;
		if( service != null ){
			serv = new rowk.lyxar.envelope.imp.KService();		
			serv.id = getIfNotNull(service, ID_KEY);
			serv.name = getIfNotNull(service, NAME_KEY);
			serv.urlString = getIfNotNull(service, HOST_KEY);
		}
		return serv;
	}
	
	
	
	
	static Version mapVersion( String json ){	
		
		Version version = new Version();
		if( json != null && json.length() != 0){
			
			JSONObject jsonObject;
			try{
				 jsonObject = new JSONObject(json);
			}catch( JSONException ex ){
				throw new IllegalArgumentException( "The parsed version is malformed" );
			}		
			version.setBrokerKey( getIfNotNull(jsonObject, Common.Keys.BROKER_VERSION_KEY));
		}
		return version;
	}
	
	
	
	
	static String getIfNotNull( JSONObject json, String name ){
		if(!json.isNull(name))
			try {
				return json.getString(name);
			} catch (JSONException e) {
				return "";
			}
		return "";
	}
	
}
